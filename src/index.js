import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BookProvider } from './Helpers/BookContext'
import { DarkProvider } from './Helpers/DarkModeContext';


ReactDOM.render(
    <React.StrictMode>
        <BookProvider>
            <DarkProvider>
                <App />
            </DarkProvider>
        </BookProvider>
    </React.StrictMode>,
    document.getElementById( 'root' )
);

// ReactDOM.render( <DraggableList items={ 'Lorem ipsum dolor sit'.split( ' ' ) } />, document.getElementById( 'root' ) )



// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
