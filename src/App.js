import React, { useContext } from 'react'
import './App.css';
import { ThemeProvider } from '@material-ui/core';
import { ColorDark, ColorLight } from './Component/ThemeProvider/Color';
import Layout from './Component/Layout';
import Book from './Component/Book';
import { DarkModeContext } from './Helpers/DarkModeContext';


function App() {
  const theme = useContext(DarkModeContext);
  return (
    <ThemeProvider theme={theme === 'dark' ? ColorDark : ColorLight}>
      <Layout>
        <Book />
      </Layout>
    </ThemeProvider>
  );
}

export default App;