import { createMuiTheme } from '@material-ui/core/styles'


export const ColorDark = createMuiTheme( {
    palette: {
        primary: {
            main: '#2f4f4f'
        },
        secondary: {
            main: '#ffffff'
        },
        initial: {
            main: 'rgb( 57, 109, 109 )'
        },
        textPrimary: {
            main: '#B2B2B2',
        },
        inherit: {
            main: '#fcb711',
        },
        light: {
            main: 'rgba(80, 80, 80, 0.15)'
        },
        dark: {
            main: '#041f1f'
        },

    }
} );


export const ColorLight = createMuiTheme( {
    palette: {
        primary: {
            main: '#d5dbdb'
        },
        secondary: {
            main: '#041f1f'
        },
        initial: {
            main: '#829595',
        },
        textPrimary: {
            main: '#2f4f4f'
        },
        inherit: {
            main: '#b0800b',
        },
        light: {
            main: 'rgba(0, 0, 0, 0.15)'
        },
        dark: {
            main: '#829595'
        },

    }
} );