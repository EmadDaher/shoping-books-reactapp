import React, { useEffect, useContext, useState } from 'react'
import CardComponent from '../CardComponent'
import Aos from "aos";
import "aos/dist/aos.css";
import { InputContext, BookContext } from '../../Helpers/BookContext'
import Spinner from '../Spinner'
import NotFound from '../NotFound'


const Book = () =>
{
    const currentPosts = useContext( BookContext )
    const { loading, filter, setLoading } = useContext( InputContext )
    const [ temp, setTemp ] = useState( [] )

    useEffect( () =>
    {
        Aos.init( { duration: 1000, offset: 50, once: true } );
    }, [] )

    useEffect( () =>
    {
        setTemp( currentPosts.filter( e => e.volumeInfo.title.toUpperCase().indexOf( filter.toUpperCase() ) !== -1 ) )
        // setLoading( false )
    }, [ filter, currentPosts ] )

    return (
        <>
            {
                !loading ?
                    temp.length > 0 ?
                        temp.map( elements =>
                        {
                            return <CardComponent key={ elements.id } { ...elements.volumeInfo } />
                        } )
                        : <NotFound filter={ `"${ filter.toUpperCase() }" -` } />
                    :
                    <Spinner />
            }
        </>
    )
}

export default Book;
