import React, { useContext } from 'react'
import { DarkModeContext, ChangeDarkMode } from '../../Helpers/DarkModeContext'
import Brightness4Icon from '@material-ui/icons/Brightness4';
import Brightness7Icon from '@material-ui/icons/Brightness7';
import { IconButton } from '@material-ui/core'
import { ColorDark, ColorLight } from '../ThemeProvider/Color'

const ToggleTheme = () =>
{
    const toggleTheme = useContext( ChangeDarkMode );
    const theme = useContext( DarkModeContext );

    return (
        <IconButton onClick={ toggleTheme } style={ { color: theme === 'dark' ? ColorDark.palette.secondary.main : ColorLight.palette.secondary.main } } aria-label="upload picture" component="span">
            { theme === 'dark' ? <Brightness4Icon /> : <Brightness7Icon /> }
        </IconButton>
    )
}

export default ToggleTheme

