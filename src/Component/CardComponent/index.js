import React, { useEffect, useContext } from 'react';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import useStyles from './styles'
import photoNotFound from '../Book/image/photoNotFound.png'
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import Tooltip from '@material-ui/core/Tooltip';
import Aos from "aos";
import "aos/dist/aos.css";
import { DarkModeContext } from '../../Helpers/DarkModeContext';


const CardComponent = ( { title, previewLink, imageLinks, description, authors, pageCount, publishedDate, publisher } ) =>
{
    const theme = useContext( DarkModeContext );
    const classes = useStyles();

    useEffect( () =>
    {
        Aos.init( { duration: 1000, offset: 50, once: true } );
    }, [ theme ] )

    return ( <>
        {
            !!imageLinks
                ?
                <CardActionArea className={ classes.book } data-aos="fade-up" data-aos-easing="ease-in-out-back" data-aos-once='false'>
                    <div className={ classes.bookImage } data-aos="zoom-out" data-aos-duration='1500'>
                        <a className={ classes.link } href={ previewLink } >
                            <img src={ imageLinks?.thumbnail || photoNotFound } className={ classes.image } alt="Image Not Found" />
                        </a>
                    </div>
                    <CardContent className={ classes.bookInfo }>
                        <Tooltip title={ title } placement="top">
                            <Typography gutterBottom variant="h5" component="h2" className={ classes.title } data-aos="fade-right" data-aos-duration="1800">
                                { title }
                            </Typography>
                        </Tooltip>
                        <hr style={ { width: '80%' } } />
                        {
                            !!authors ?
                                <Tooltip title={ authors } placement="top">
                                    <Typography gutterBottom component="h4" className={ classes.address }>
                                        { authors }
                                    </Typography>
                                </Tooltip>
                                :
                                <Tooltip title="Authors Unknown" placement="top">
                                    <Typography gutterBottom component="h4" className={ classes.address }>
                                        Authors Unknown { "  " }
                                    </Typography>
                                </Tooltip>
                        }
                        {
                            !!pageCount ?
                                <Typography gutterBottom component="h6" className={ classes.pageCount }>
                                    Pages  - { pageCount }
                                </Typography>
                                :
                                <Typography gutterBottom component="h6" className={ classes.pageCount }>
                                    xxx-xx
                                </Typography>
                        }
                        {
                            !!publisher ?
                                <Tooltip title={ publisher + " - " + publishedDate }>
                                    <Typography gutterBottom variant="overline" component="h5" className={ classes.publisher }>
                                        { publisher }{ "  " }
                                        {
                                            !!publishedDate ?
                                                <Tooltip title={ publishedDate }>
                                                    <n className={ classes.data }>{ publishedDate }</n>
                                                </Tooltip>
                                                :
                                                <Tooltip title="data unlKnwon">
                                                    <n className={ classes.data }>xx-xx-xxxx</n>
                                                </Tooltip>
                                        }
                                    </Typography>
                                </Tooltip>
                                :
                                <Typography gutterBottom variant="overline" component="h5" className={ classes.publisher }>
                                    Publisher Unknown { "  " }
                                    {
                                        !!publishedDate ?
                                            <Tooltip title={ publishedDate }>
                                                <n className={ classes.data }>{ publishedDate }</n>
                                            </Tooltip>
                                            :
                                            <Tooltip title="data unlKnwon">
                                                <n className={ classes.data }>xx-xx-xxxx</n>
                                            </Tooltip>
                                    }
                                </Typography>
                        }
                        <div className={ classes.forShowOverview }>
                            <Typography className={ classes.headerAll }>
                                {
                                    !!description ?
                                        <>
                                            <Typography className={ classes.headerAll }>
                                                Overview
                                            </Typography>
                                            <h3 className={ classes.headerAll }><ArrowDownwardIcon fontSize="small" className={ classes.icon } /> </h3>
                                        </> : null
                                }

                            </Typography>
                            {
                                !!description
                                    ?
                                    <Typography variant="body2" color="textSecondary" component="p" className={ classes.overview }>
                                        {
                                            <>
                                                <h3 className={ classes.header }>Overview </h3>
                                                { description }
                                            </>
                                        }

                                    </Typography>
                                    :
                                    null
                            }
                        </div>
                    </CardContent>
                </CardActionArea>
                :
                null
        }

    </> )
}

export default CardComponent