import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles( ( theme ) => ( {
    book: {
        position: 'relative',
        overflow: 'hidden',
        height: '470px',
        backgroundColor: theme.palette.primary.main,
        width: 'calc(400px - 10%)',
        margin: '1.2rem',
        borderRadius: '15px',
        boxShadow: `10px 10px 15px 5px ${ theme.palette.light.main }`,
        transition: '0.3s all ease-in-out',
        [ '@media (min-device-width:320px)' && [ '@media (max-device-width:640px)' ] ]: {
            margin: '50px',
            width: '60%',
            fontSize: '15px',
        },
        '&:hover $bookImage': {
            dataAos: 'zoom-in'
        },
        [ '@media (max-device-width:320px)' ]: {
            height: '400px',
            marginTop: '20px',
        },
        [ '@media (min-device-width:2000px)' ]: {
            width: 'calc(600px - 10%)',
            height: '585px',
            fontSize: '25px',
            margin: '2rem',
        },
    },


    bookImage: {
        display: 'flex',
        justifyContent: 'center',
        height: '225px',
        margin: '1rem 0',
        [ '@media (min-device-width:600px)' && [ '@media (max-device-width:1024px)' ] ]: {
            width: '50%',
        },
        [ '@media (min-device-width:2000px)' ]: {
            height: '300px',
        },
    },
    link: {
        [ '@media (min-device-width:600px)' && [ '@media (max-device-width:1024px)' ] ]: {
            marginLeft: '100%'
        },
        [ '@media (max-device-width:320px)' ]: {
            height: '70%',
        },
        [ '@media (min-device-width:2000px)' ]: {
            height: '90%',
        },
    },
    image: {
        minWidth: '100%',
        height: '112%',
        borderRadius: '5px',
        boxShadow: '10px 10px 5px rgba(28, 49, 66, 0.349)',
        zindex: '5',
        transition: '0.3s all ease-in-out',
        '&:hover': {
            transform: 'scale(1.1) ',
        },
    },
    bookInfo: {
        display: 'flex',
        flexDirection: 'column',
        padding: '1rem',
        height: '245px',
        color: theme.palette.secondary.main,
        [ '@media (max-device-width:320px)' ]: {
            marginTop: '-70px'
        },
    },
    forShowOverview: {
        '&:hover $overview': {
            height: '50%',
            transform: 'translateY(0%)',
            '&::-webkit-scrollbar': {
                width: '6px',
            },
            backgroundColor: 'rgb(238, 200, 165)',
            color: 'darkslategrey',
            padding: '8px 20px',

            '& $icon': {
                display: 'none'
            },

        },
    },
    address: {
        height: '21%',
        overflow: 'auto',
        color: theme.palette.inherit.main,
        '&::-webkit-scrollbar': {
            width: '2px',
            height: '15px',
            backgroundColor: theme.palette.secondary.main
        },
        [ '@media (min-device-width:2000px)' ]: {
            height: '30%',
            fontSize: '25px'
        },
    },

    title: {
        height: '20%',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        [ '@media (min-device-width:2000px)' ]: {
            fontSize: '28px'
        },
    },

    pageCount: {
        color: theme.palette.textPrimary.main,
        display: 'flex',
        justifySelf: 'flex-end',
        height: '5%',


        '&:hover $overview': {
            transform: 'translateY(0%)',
            '&::-webkit-scrollbar': {
                width: '6px',
            },
            backgroundColor: 'rgb(238, 200, 165)',
            color: 'darkslategrey',
            padding: '8px 20px',

            '& $icon': {
                display: 'none'
            },
        },
        [ '@media (min-device-width:2000px)' ]: {
            fontSize: '18px',
            height: '8%',
        },
    },
    publisher: {
        height: '15%',
        width: '100%',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        overflow: 'hidden',

        '&:hover $overview': {
            transform: 'translateY(0%)',
            '&::-webkit-scrollbar': {
                width: '6px',
            },
            backgroundColor: 'rgb(238, 200, 165)',
            color: 'darkslategrey',
            padding: '8px 20px',

            '& $icon': {
                display: 'none'
            },
        },
        [ '@media (min-device-width:2000px)' ]: {
            fontSize: '20px',
            height: '30%',
        },
    },
    data: {
        color: theme.palette.dark.main,
        width: '10%',
    },
    overview: {
        backgroundColor: 'rgb(238, 200, 165)',
        padding: '8px 20px',
        maxHeight: '50%',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        overflow: 'auto',
        transform: 'translateY(102%)',
        transition: 'transform 0.3s ease-in-out',
        borderRadius: '10px 10px 5px 5px',

        '&::-webkit-scrollbar': {
            width: 0,
        },

        '&::-webkit-scrollbar-thumb': {
            borderRadius: '25px',
            backgroundColor: 'rgb(67, 119, 119)',
        },
        [ '@media (min-device-width:2000px)' ]: {
            fontSize: '22px',
        },
    },
    overviewDiv: {
        color: theme.palette.secondary.main,

        '&:hover $overview': {
            transform: 'translateY(0%)',
            '&::-webkit-scrollbar': {
                width: '6px',
            },
            backgroundColor: 'rgb(238, 200, 165)',
            color: 'darkslategrey',
            padding: '8px 20px',

            '& $icon': {
                display: 'none'
            },
        },
    },
    header: {
        marginTop: -2,
        marginLeft: '40%',
        [ '@media (min-device-width:2000px)' ]: {
            fontSize: '25px',
            marginLeft: '30%',
        },
    },
    headerAll: {
        marginTop: -6,
        marginBottom: 'auto',
        height: '100%',
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        color: theme.palette.dark.main,
        [ '@media (min-device-width:2000px)' ]: {
        },
    },
} ) );

export default useStyles