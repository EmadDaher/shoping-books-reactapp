import React, { Fragment, useContext } from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import imgLight from '../Header/Emad-designstyle-kiddo-l.png'
import imgDark from '../Header/Emad-designstyle-boots-2.png'
import { DarkModeContext } from '../../Helpers/DarkModeContext'
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles( ( theme ) => ( {
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
        backgroundColor: theme.palette.primary.main,
    },
    CircularProgress: {
        color: theme.palette.secondary.main,
    }
} ) );


function Spinner ()
{
    const classes = useStyles();
    const theme = useContext( DarkModeContext );

    return (
        <Fragment>
            <Backdrop className={ classes.backdrop } open>
                <CircularProgress className={ classes.CircularProgress } />
                {
                    theme === "dark" ?
                        <div style={ { marginTop: '10%', marginLeft: '-8%', opacity: '0.8' } }>
                            <img src={ imgDark } data-aos="zoom-in" data-aos-duration='1500' alt="Emad" />
                        </div>
                        :
                        <div style={ { marginTop: '10%', marginLeft: '-8%', opacity: '0.8' } }>
                            <img src={ imgLight } data-aos="zoom-in" data-aos-duration='1500' alt="Emad" />
                        </div>
                }
            </Backdrop>
        </Fragment>
    )
}

export default Spinner;
