import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles( ( theme ) => ( {
    searchForm: {
        display: 'flex',
        justifySelf: 'flext-start',
        alignSelf: 'center',
        boxShadow: '0 5px 25px #263238cc',
        width: '80%',
        marginRight: '10px',
        borderRadius: '4px',
        transition: '0.4s all ease-in-out',
        backgroundColor: '#ffffff',
        '&:hover': {
            width: '100%',
            marginRight: '50px',
            backgroundColor: '#2f4f4f',
            border: '1px solid #ffffff',
            borderTopLeftRadius: '4px',
        },
        borderTopRightRadius: '8px',
        borderBottomRightRadius: '8px',
        borderTopLeftRadius: '30px',
    },
    navSearchFormInput: {
        outline: 0,
        height: '42px',
        width: '100%',
        lineHeight: '42px',
        padding: '0 16px',
        backgroundColor: 'rgba( 255, 255, 255, 0.8 )',
        color: '#212121',
        border: 0,
        borderRadius: '4px 0 0 4px',
        fontFamily: 'cursive',
        // textDecoration: 'underline',
        '&:focus': {
            outline: 0,
            backgroundColor: '#fff'
        },
        '&::placeholder': {
            color: 'black'
        },

        [ theme.breakpoints.down( 'sm' ) ]: {
            width: '100%'
        },

    },
    SearchIcon: {
        color: theme.palette.secondary.main,
        zIndex: '1',
    },
    navSearchFormButton: {
        backgroundColor: theme.palette.initial.main,
        width: '50px',
        height: '42px',
        border: 0,
        color: '#fff',
        fontSize: '16px',
        textShadow: '0 1px 1px rgba(0, 0, 0, 0.2)',
        transition: 'background-color 0.4s ease',
        borderRadius: '4px 4px 4px 4px',
        padding: theme.spacing( 0.5, 1, 3 ),
        boxShadow: '0 5px 25px #263238cc',
        zIndex: '1',
        '&:hover': {
            backgroundColor: theme.palette.primary.main,
        },
        [ theme.breakpoints.down( 'xs' ) ]: {
            width: '35px',
            padding: theme.spacing( 0.5, 0, 3 ),
            fontSize: 'small'
        }
    },
    noBorder: {
        border: "none",
    },
    resize: {
        fontFamily: 'cursive',
    },

} ) );

export default useStyles