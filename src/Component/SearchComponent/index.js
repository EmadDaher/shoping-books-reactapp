import React, { useContext } from 'react'
import useStyles from './styles'
import SearchIcon from '@material-ui/icons/Search';
import { InputContext } from '../../Helpers/BookContext'
import { TextField } from '@material-ui/core'

const SearchComponent = ( props ) =>
{
    const inputContext = useContext( InputContext )
    const classes = useStyles();


    return (

        //search for API : ----

        // <form onSubmit={ inputContext.getSearch } className={ classes.searchForm }>

        //     <input onChange={ ( e ) => inputContext.setSearch( e.target.value ) }
        //         placeholder="Book, Author"
        //         className={ classes.navSearchFormInput } type="text"
        //         required
        //         value={ inputContext.search }
        //         autoComplete="false"
        //     />
        //     <button title="Search" type="submit" className={ classes.navSearchFormButton } >
        //         <SearchIcon className={ classes.SearchIcon } fontSize="large" />
        //     </button>
        // </form>


        //Search by frontend using onSubmit :----

        <form onSubmit={ ( e ) => inputContext.handleSearchChangeForSubmit( e ) } className={ classes.searchForm }>

            <TextField
                onChange={ e => inputContext.setInputVal( e.target.value ) }
                placeholder="Find a Title"
                className={ classes.navSearchFormInput }
                type="text"
                required
                autoComplete="false"
                value={ inputContext.inputVal }
                variant="outlined"
                size="small"
                name="filter1"
                // helperText={ inputContext.inputVal === "" ?
                //     "Please enter the Title you wish to search for"
                //     :
                //     inputContext.helperText }
                InputProps={ {
                    classes: { notchedOutline: classes.noBorder, input: classes.resize },
                } }
            />
            <button title="Search" type="submit" className={ classes.navSearchFormButton } >
                <SearchIcon className={ classes.SearchIcon } fontSize="large" />
            </button>
        </form>


        //Search by frontend using onChange : ----

        // <div className={ classes.searchForm }>

        //     <TextField
        //         onChange={ inputContext.handleSearchChange }
        //         // onChange={ e => setFilter1( e.target.value ) }
        //         // onBlur={ inputContext.handleSearchChange }
        //         placeholder="Find a Title"
        //         className={ classes.navSearchFormInput }
        //         type="text"
        //         required
        //         autoComplete="false"
        //         // value={ inputContext.filter }
        //         variant="outlined"
        //         size="small"
        //         // name="filter1"
        //         // helperText={ "Add an existing id or select " }
        //         InputProps={ {
        //             classes: { notchedOutline: classes.noBorder, input: classes.resize },
        //         } }
        //     />

        //     <div title="Search" type="submit" className={ classes.navSearchFormButton } >
        //         <SearchIcon className={ classes.SearchIcon } fontSize="large" />
        //     </div>
        // </div>
    )
}

export default SearchComponent