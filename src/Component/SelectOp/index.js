import React, { useContext } from 'react';
import './styles.css'
import { InputContext } from '../../Helpers/BookContext'

const SelectOp = () =>
{
    const inputContext = useContext( InputContext );

    return (
        <div className="box">
            <select value={ inputContext.bookFilter } onChange={ ( e ) =>
            {
                inputContext.setLoading( true )
                inputContext.setBookFilter( e.target.value )
            } }>
                <option value="">All</option>
                <option value="free-ebooks">Free</option>
                <option value="paid-ebooks">Paid</option>
            </select>
        </div>
    );
}

export default SelectOp



