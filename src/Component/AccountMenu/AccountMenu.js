import { makeStyles } from '@material-ui/core/styles';
import AppsIcon from '@mui/icons-material/Apps';
import LocalLibraryIcon from '@mui/icons-material/LocalLibrary';
import MenuBookIcon from '@mui/icons-material/MenuBook';
import TranslateIcon from '@mui/icons-material/Translate';
import Avatar from '@mui/material/Avatar';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import * as React from 'react';
import { InputContext } from '../../Helpers/BookContext';
import { DarkModeContext } from '../../Helpers/DarkModeContext';
import { ColorDark, ColorLight } from '../ThemeProvider/Color';


const useStyles = makeStyles( ( theme ) => ( {
    menu: {
        "& .MuiPaper-root": {
            backgroundColor: theme.palette.primary.main,
            color: theme.palette.secondary.main,
        },
    },
    icon: {
        color: theme.palette.inherit.main,
    },
} ) )

export default function AccountMenu ()
{
    const classes = useStyles();
    const theme = React.useContext( DarkModeContext );
    const { changeLangFr, changeLangEn, changeMagazines, changeBooks } = React.useContext( InputContext );
    const [ anchorEl, setAnchorEl ] = React.useState( null );
    const open = Boolean( anchorEl );
    const handleClick = ( event ) =>
    {
        setAnchorEl( event.currentTarget );
    };
    const handleClose = () =>
    {
        setAnchorEl( null );
    };

    return (
        <React.Fragment>
            <IconButton onClick={ handleClick } size="small">
                <Avatar className="" style={ { backgroundColor: 'transparent' } } sx={ { width: 32, height: 32 } }><AppsIcon style={ { color: theme === 'dark' ? ColorDark.palette.secondary.main : ColorLight.palette.secondary.main } } /></Avatar>
            </IconButton>
            <Menu
                className={ classes.menu }
                anchorEl={ anchorEl }
                open={ open }
                onClose={ handleClose }
                onClick={ handleClose }
                PaperProps={ {
                    elevation: 0,
                    sx: {
                        overflow: 'visible',
                        filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                        mt: 1.5,
                        '& .MuiAvatar-root': {
                            width: 32,
                            height: 32,
                            ml: -0.5,
                            mr: 1,
                        },
                        '&:before': {
                            content: '""',
                            display: 'block',
                            position: 'absolute',
                            top: 0,
                            right: 14,
                            width: 10,
                            height: 10,
                            // bgcolor: 'background.paper',
                            bgcolor: theme === 'dark' ? ColorDark.palette.primary.main : ColorLight.palette.primary.main,
                            transform: 'translateY(-50%) rotate(45deg)',
                            zIndex: 0,
                        },
                    },
                } }
                transformOrigin={ { horizontal: 'right', vertical: 'top' } }
                anchorOrigin={ { horizontal: 'right', vertical: 'bottom' } }
            >
                <MenuItem
                    // onMouseEnter={ ( e ) => e.target.style.backgroundColor = '#fef0cf' }
                    // onMouseLeave={ ( e ) => e.target.style.backgroundColor = '#ffffff' }
                    onClick={ changeLangEn }
                >
                    <ListItemIcon>
                        <TranslateIcon className={ classes.icon } fontSize="small" />{ ' ' }
                    </ListItemIcon>
                    English
                </MenuItem>

                <MenuItem
                    // onMouseEnter={ ( e ) => e.target.style.backgroundColor = 'red' }
                    // onMouseLeave={ ( e ) => e.target.style.backgroundColor = '#ffffff' }
                    onClick={ changeLangFr }
                >
                    <ListItemIcon>
                        <TranslateIcon className={ classes.icon } fontSize="small" />{ ' ' }
                    </ListItemIcon>
                    Frances
                </MenuItem>

                <Divider />

                <MenuItem
                    // onMouseEnter={ ( e ) => e.target.style.backgroundColor = 'red' }
                    // onMouseLeave={ ( e ) => e.target.style.backgroundColor = '#ffffff' }
                    onClick={ changeBooks }
                >
                    <ListItemIcon>
                        <MenuBookIcon className={ classes.icon } fontSize="small" />{ "  " }
                    </ListItemIcon>
                    Books
                </MenuItem>

                <MenuItem
                    // onMouseEnter={ ( e ) => e.target.style.backgroundColor = 'red' }
                    // onMouseLeave={ ( e ) => e.target.style.backgroundColor = '#ffffff' }
                    onClick={ changeMagazines }
                >
                    <ListItemIcon>
                        <LocalLibraryIcon className={ classes.icon } fontSize="small" />
                    </ListItemIcon>
                    magazines
                </MenuItem>
            </Menu>
        </React.Fragment>
    );
}
