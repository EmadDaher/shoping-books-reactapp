import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles( ( theme ) => ( {
    container: {
        marginTop: '1px',
        width: '100%',
        height: '79%',
        position: 'absolute',
        paddingTop: '10%',
        paddingLeft: '7%',
    },
    body: {
        width: '500px',
        backgroundColor: '#0286a6',
        borderRadius: '50px',
        padding: '5px 5px 5px 10px',
        color: '#ffffff',
        fontSize: '18px',
        fontWeight: 'bold',
        lineHeight: '20px',
        letterSpacing: '0.5px',
        textShadow: '0.5px 0.5px #000000',
        textAlign: 'center'
    },
    filterText: {
        fontSize: '20px',
        textDecoration: 'underLine',
        color: '#0286a6',
    },
    title: {
        fontSize: '30px',
    },
    goBack: {
        color: theme.palette.dark.main,
        fontFamily: 'cursive',
        textDecoration: 'underLine',
        cursor: ' url( "https://s3-us-west-2.amazonaws.com/s.cdpn.io/9632/sad.png" ),auto'

    }
} ) );

export default useStyles