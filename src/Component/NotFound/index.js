import React, { useContext } from 'react'
import useStyles from './styles'
import { DarkModeContext } from '../../Helpers/DarkModeContext'
import { InputContext } from '../../Helpers/BookContext'
import LogoDark from '../Header/Emad-designstyle-boots-2.png'
import Logo from '../Header/Emad-designstyle-kiddo-l.png'

const NotFound = ( { filter } ) =>
{
    const classes = useStyles();
    const theme = useContext( DarkModeContext );
    const { resetFilterAndInputVal } = useContext( InputContext );

    return (
        <div className={ classes.container }>
            <p className={ classes.title }><n className={ classes.filterText }>{ filter }</n> Not Found!!!</p>
            <p className={ classes.body }>
                This search is not found on this page. Please scroll down to search the rest of the pages
                <a onClick={ () => resetFilterAndInputVal() } className={ classes.goBack }> Go Back</a>
            </p>
            {
                theme === 'dark' ?
                    <img src={ LogoDark } data-aos="zoom-in" data-aos-duration='1500' alt="Emad" />
                    :
                    <img src={ Logo } data-aos="zoom-in" data-aos-duration='1500' alt="Emad" />
            }
        </div>
    )
}

export default NotFound
