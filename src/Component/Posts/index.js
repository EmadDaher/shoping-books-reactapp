import React, { useRef } from 'react'

const Posts = ( { posts, loading1, ulRef } ) =>
{
    if ( loading1 )
    {
        return <p>...loading</p>
    }

    return (
        <div ref={ ulRef } id="scroller">
            <ul>
                {
                    posts.map( post => (
                        <li key={ post.id }>
                            { post.title }
                        </li>
                    ) )
                }
            </ul>
        </div>
    )
}

export default Posts
