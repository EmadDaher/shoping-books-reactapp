import React from 'react'

const Paginations = ( { postsPerPage, totalPosts, paginate } ) =>
{
    const pageNumbers = [];

    for ( let i = 1; i < Math.ceil( totalPosts / postsPerPage ); i++ )
    {
        pageNumbers.push( i );
    }
    
    return (
        <nav>
            <ul>
                {
                    pageNumbers.map( number => (
                        <li onClick={ () => paginate( number ) } key={ number }>
                            { number }
                        </li>
                    ) )
                }
            </ul>
        </nav>
    )
}

export default Paginations
