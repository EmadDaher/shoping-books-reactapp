import { makeStyles, alpha } from '@material-ui/core/styles';


const useStyles = makeStyles( ( theme ) => ( {
    grow: {
        alignItems: 'baseline',
    },
    toolbar: {
        height: '130px',
        width: '100%',
        paddingBottom: '1rem',
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        backgroundColor: 'transparent',
        alignContent: 'space-around',
        position: 'fixed',
        transition: 'all 0.5s ease-out',
        zIndex: 10,
        [ theme.breakpoints.down( 'sm' ) ]: {
            paddingTop: theme.spacing( 0 ),
            alignContent: 'center',
            paddingBottom: 0,
        },
        [ theme.breakpoints.down( 'md' ) ]: {
            paddingTop: theme.spacing( 0 ),
            alignContent: 'center',
            paddingBottom: 0,
        },
        [ theme.breakpoints.down( 'lg' ) ]: {
            paddingTop: theme.spacing( 0 ),
            alignContent: 'center',
            paddingBottom: 0,
        },
        [ '@media (max-device-width:320px)' ]: {
            paddingBottom: '25px',
        },
    },
    addColor: {
        transition: 'all 0.5s ease-out',
        backgroundColor: theme.palette.primary.main,
    },
    sparate: {
        fontWeight: 'bold',
        marginTop: theme.spacing( 0.4 ),
        display: 'none',
        [ theme.breakpoints.up( 'sm' ) ]: {
            display: 'block',
        },
        color: theme.palette.secondary.main,
        transition: '0.3s all ease-in-out',
        '&:hover': {
            color: theme.palette.textPrimary.main,
        },
    },
    divTitle: {
        width: '315px',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        transition: '0.4s all ease-in-out',

        '&:hover': {
            width: '270px'
        }
    },
    menuButton: {
        marginRight: theme.spacing( 2 ),
    },
    title: {
        display: 'none',
        [ theme.breakpoints.up( 'sm' ) ]: {
            display: 'block',
        },
        color: theme.palette.secondary.main,
        transition: '0.3s all ease-in-out',
        '&:hover': {
            color: theme.palette.textPrimary.main,
        },
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: 'white',
        '&:hover': {
            backgroundColor: alpha( theme.palette.common.white, 0.25 ),
            width: '800px'
        },
        marginRight: theme.spacing( 2 ),
        marginLeft: 0,
        width: '100%',
        [ theme.breakpoints.up( 'sm' ) ]: {
            marginLeft: theme.spacing( 3 ),
            width: 'auto',
        },
        [ theme.breakpoints.down( 'xs' ) ]: {
            marginTop: theme.spacing( 2 ),
        },
    },
    search: {
        width: '500px',
        [ theme.breakpoints.down( 'sm' ) ]: {
            marginTop: theme.spacing( -2 ),
            width: '50%'
        },
        [ theme.breakpoints.down( 'xs' ) ]: {
            width: '45%'
        },
    },
    searchIcon: {
        padding: theme.spacing( 0, 2 ),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing( 1, 1, 1, 0 ),
        paddingLeft: `calc(1em + ${ theme.spacing( 4 ) }px)`,
        transition: theme.transitions.create( 'width' ),
        width: '100%',
        [ theme.breakpoints.up( 'md' ) ]: {
            width: '20ch',
        },
    },
    sectionDesktop: {
        width: '400px',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        [ theme.breakpoints.down( 'md' ) ]: {
            marginTop: '-1rem',
            width: '200px',
            marginLeft: '50%'
        },
        [ theme.breakpoints.down( 'sm' ) ]: {
            marginTop: '-1rem',
            marginLeft: '0%'
        },
        [ theme.breakpoints.down( 'xs' ) ]: {
            width: '150px'
        },
        [ '@media (max-device-width:320px)' ]: {
            width: '155px',
        },

    },
    sectionMobile: {
        display: 'none',
    },
} ) );

export default useStyles