import React, { useEffect, useContext } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import useStyles from './styles'
import SelectOp from '../SelectOp';
import SearchComponent from '../SearchComponent';
import clsx from 'clsx';
import Logo from './Emad-designstyle-kiddo-l.png'
import LogoDark from './Emad-designstyle-boots-2.png'
import { InputContext } from '../../Helpers/BookContext'
import { DarkModeContext } from '../../Helpers/DarkModeContext'
import ToggleTheme from '../ToggleTheme';
import AccountMenu from '../AccountMenu/AccountMenu';
import Aos from "aos";

const Header = ( props ) =>
{
    const inputContext = useContext( InputContext );
    const classes = useStyles();
    const theme = useContext( DarkModeContext );

    useEffect( () =>
    {
        Aos.init( { duration: 1000, offset: 50, once: true } );
    }, [] )
    return (
        <div className={ classes.grow }>
            <AppBar position="static" color="transparent" elevation={ 0 }>
                <Toolbar className={ clsx( classes.toolbar, `${ inputContext.navBackground && classes.addColor }` ) } color="transparent">

                    <div className={ classes.divTitle } >
                        <div className={ classes.divTitle } data-aos="fade-up" data-aos-easing="ease-in-out-back" data-aos-once='false'>
                            {
                                theme === 'dark' ?
                                    <img src={ LogoDark } data-aos="zoom-in" data-aos-duration='1500' alt="Emad" />
                                    :
                                    <img src={ Logo } data-aos="zoom-in" data-aos-duration='1500' alt="Emad" />
                            }
                        </div>
                    </div>
                    <div className={ classes.search }>
                        <SearchComponent />

                    </div>
                    <div className={ classes.sectionDesktop }>
                        <div style={ { width: '64%' } }>
                            <SelectOp />
                        </div>
                        <ToggleTheme />
                        <AccountMenu />
                    </div>
                    <div className={ classes.sectionMobile }>
                        <h4>Fillter for mobile</h4>
                    </div>
                </Toolbar>
            </AppBar>
        </div>
    );
}

export default Header
