import React from 'react'
import Header from '../Header';
import useStyles from './styles'

const Layout = ( props ) =>
{
    const classes = useStyles();

    return (
        <div className={ classes.app }>
            <Header />
            <main className={ classes.main }>
                { props.children }
            </main>

        </div>
    )
}

export default Layout
