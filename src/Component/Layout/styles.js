import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles( ( theme ) => ( {
    app: {
        backgroundColor: theme.palette.initial.main,
    },
    main: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
        marginTop: '10%',
        [ '@media (min-device-width:650px)' && [ '@media (max-device-width:960px)' ] ]: {
            marginTop: '120px',
        },
        [ theme.breakpoints.down( 'md' ) ]: {
            marginTop: '7.5rem'
        },
    }
} ) );

export default useStyles