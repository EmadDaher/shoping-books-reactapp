import { useState, useEffect, createContext } from 'react';
import axios from 'axios';


export const BookContext = createContext();
export const InputContext = createContext();

export const BookProvider = ( { children } ) =>
{
    const APP_KEY = "AIzaSyBswzUbSbQ-eroJXoLtzRlAG8LWIMIr0k8";
    const [ navBackground, setNavBackground ] = useState( false )
    const [ bookFilter, setBookFilter ] = useState( "" )
    const [ search, setSearch ] = useState( "" );
    const [ query, setQuery ] = useState( "React" );
    const [ loading, setLoading ] = useState( true );
    const [ language, setLanguage ] = useState( "en" );
    const [ printType, setPrintType ] = useState( "books" );
    const URL = `https://www.googleapis.com/books/v1/volumes?q=${ query }${ bookFilter && `&filter=${ bookFilter }` }&key=${ APP_KEY }&maxResults=40&langRestrict=${ language }&printType=${ printType }`;
    const [ book, setBook ] = useState( [] );
    const [ currentPage, setCurrentPage ] = useState( 1 );
    const [ postsPerPage ] = useState( 10 );
    const [ filter, setFilter ] = useState( "" );
    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    const [ currentPosts, setCurrentPosts ] = useState( [] )
    const [ inputVal, setInputVal ] = useState( '' )
    // const [ helperText, setHelperText ] = useState( "Please press the enter button when you have finished typing the Title you want to search for" )


    useEffect( () =>
    {
        getBooks();
        window.addEventListener( 'scroll', controllNavabr )
        return () =>
        {
            window.removeEventListener( 'scroll', controllNavabr )
        }


    }, [ query, bookFilter, language, printType ] )

    useEffect( () =>
    {
        let temp = [ ...currentPosts, ...book.slice( indexOfFirstPost, indexOfLastPost ) ];
        setCurrentPosts( temp )
        if ( ( window.innerHeight + window.scrollY ) >= document.body.offsetHeight )
            window.addEventListener( 'scroll', controllScroll )
        else window.removeEventListener( 'scroll', controllScroll )
        return () =>
        {
            window.removeEventListener( 'scroll', controllScroll )
        }

    }, [ currentPage ] )


    const controllScroll = () =>
    {
        if ( ( window.innerHeight + window.scrollY ) >= document.body.offsetHeight )
        {
            setCurrentPage( currentPage + 1 );
        }

    }

    const changeLangFr = () =>
    {
        setLoading( true )
        setLanguage( "fr" )
    }

    const changeLangEn = () =>
    {
        setLoading( true )
        setLanguage( "en" )
    }

    const changeBooks = () =>
    {
        setLoading( true )
        setPrintType( "books" )
    }

    const changeMagazines = () =>
    {
        setLoading( true )
        setPrintType( "magazines " )
    }

    const controllNavabr = () =>
    {

        window.scrollY > 126 ? setNavBackground( true ) : setNavBackground( false );

    }

    const getBooks = async () =>
    {

        if ( !book ) return setQuery( "React" );

        const { data } = await axios.get( URL );
        setBook( data.items );
        setLoading( false );
        setCurrentPosts( data.items.slice( indexOfFirstPost, indexOfLastPost ) );
    };

    const getSearch = ( e ) =>
    {
        setLoading( true );
        e.preventDefault();
        setQuery( search );
        setSearch( "" );

    };

    // const handleSearchChange = ( e ) =>
    // {
    //     setFilter( e.target.value );
    // }

    const resetFilterAndInputVal = () =>
    {
        setFilter( "" );
        setInputVal( "" );
    }
    const handleSearchChangeForSubmit = ( e ) =>
    {
        e.preventDefault();
        setFilter( inputVal );
        // setHelperText( "" )
    }

    useEffect( () =>
    {
        if ( inputVal === "" )
        {
            setFilter( "" )
        }
    }, [ inputVal ] )

    return (
        <BookContext.Provider value={ currentPosts } >
            <InputContext.Provider value={ { resetFilterAndInputVal, inputVal, setInputVal, handleSearchChangeForSubmit, setFilter, filter, currentPage, changeMagazines, changeBooks, changeLangFr, changeLangEn, setLoading, loading, search, setSearch, getSearch, navBackground, setBookFilter, bookFilter } } >

                { children }

            </InputContext.Provider>
        </BookContext.Provider>
    )
}