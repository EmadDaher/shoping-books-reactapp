import { useState, createContext, useEffect } from 'react';

export const DarkModeContext = createContext();
export const ChangeDarkMode = createContext();

export const DarkProvider = ( { children } ) =>
{
    const toggleTheme = () =>
    {
        setTheme( ( cur ) => ( cur === 'dark' ? 'light' : 'dark' ) )
    }

    //use Local Stoarge
    const useLocalState = ( key, defaultValue ) =>
    {
        const [ value, setValue ] = useState( () =>
        {
            const storedValue = localStorage.getItem( key );
            return storedValue === null || undefined ? defaultValue : JSON.parse( storedValue );
        } );

        useEffect( () =>
        {
            const listener = ( e ) =>
            {
                if ( e.storageArea === localStorage && e.key === key )
                {
                    setValue( JSON.parse( e.newValue ) );
                }
            }

            window.addEventListener( "storage", listener )

            return () =>
            {
                window.removeEventListener( "storage", listener )
            }

        }, [ key ] )

        const setValueInLocalStoarge = ( newValue ) =>
        {
            setValue( ( currentValue ) =>
            {
                const result = typeof newValue === "function" ? newValue( currentValue ) : newValue
                localStorage.setItem( key, JSON.stringify( result ) );
                return result;
            } )
        }

        return [ value, setValueInLocalStoarge ]
    };

    const [ theme, setTheme ] = useLocalState( 'theme', 'light' )

    return (
        <DarkModeContext.Provider value={ theme } >
            <ChangeDarkMode.Provider value={ toggleTheme }>
                { children }
            </ChangeDarkMode.Provider>
        </DarkModeContext.Provider>
    )
}